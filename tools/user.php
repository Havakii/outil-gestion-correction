<?php




function get_project_user($idproject)
{
    global $cnx;
    $sth = $cnx->prepare("
        SELECT * 
        FROM participations
        JOIN users
            ON participations.FK_user = users.ID
        WHERE FK_project = :idproject
    ");
    $sth->execute(["idproject" => $idproject]);
    return $sth->fetchAll();
}



function get_user($id)
{
    global $cnx;
    $sth = $cnx->prepare("SELECT * FROM users WHERE ID = :id;");
    $sth->execute(["id" => $id]);
    return $sth->fetchAll();
}



function get_all_users()
{
    global $cnx;
    $sth = $cnx->prepare("SELECT * FROM users;");
    $sth->execute();
    return $sth->fetchAll();
}



function add_user($firstname, $lastname, $role)
{
    global $cnx;
    $sth = $cnx->prepare("INSERT INTO users (firstname, lastname, role) VALUES (:firstname, :lastname, :role);");
    $sth->execute(["firstname" => $firstname, "lastname" => $lastname, "role" => $role]);
    return $cnx->lastInsertId();
}



function delete_user($iduser)
{
    global $cnx;
    $sth = $cnx->prepare("DELETE FROM users WHERE id = :iduser;");
    $hava = $sth->execute(["iduser" => $iduser]);
    if ($hava) {
        $sth = $cnx->prepare("DELETE FROM participations WHERE FK_user = :iduser;");
        $hava2 = $sth->execute(["iduser" => $iduser]);
        return $hava2;
    } else {
        return false;
    }
}




function update_user($id, $firstname, $lastname, $role)
{
    global $cnx;
    $sth = $cnx->prepare("UPDATE users SET firstname = :firstname, lastname = :lastname, role = :role WHERE id = :id;");
    return $sth->execute(["id" => $id, "firstname" => $firstname, "lastname" => $lastname, "role" => $role]);
}



function print_user($user)
{

    switch ($user['role']) {
        case "développeur":
            echo ' <span class="glyphicon glyphicon-wrench"></span>';
            break;

        case "client":
            echo ' <span class="glyphicon glyphicon-euro"></span>';
            break;

        default:
            echo ' <span class="glyphicon glyphicon-question-sign"></span>';
    }

    echo " " . $user['firstname'] . " " . $user['lastname'] . " ";
}

function checkUser($id)
{
    global $cnx;
    $sth = $cnx->prepare("SELECT * FROM participations WHERE FK_user = :id;");
    return $sth->execute(["id" => $id]);
}
